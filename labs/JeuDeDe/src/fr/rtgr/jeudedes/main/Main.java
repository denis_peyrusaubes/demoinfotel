package fr.rtgr.jeudedes.main;
import fr.rtgr.jeudedes.modele.JeuDeDes;

public class Main {
	// "main" est le point de d�part d�ex�cution dans un programme Java
	public static void main(String[] args) {
		// Demo 
		JeuDeDes jeuDeDes = new JeuDeDes(5);
		jeuDeDes.jouer();
	}
}
