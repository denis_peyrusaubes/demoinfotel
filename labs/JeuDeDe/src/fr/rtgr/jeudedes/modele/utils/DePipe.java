package fr.rtgr.jeudedes.modele.utils;
import fr.rtgr.jeudedes.modele.De;

public class DePipe extends De {
	private int facePipee;

	@Override
	public void lancer() {
		super.lancer();
		if (Math.random() >= 0.5)
			setValeurFace(facePipee);
	}

	public void setFacePipee(int facePipee) {
		this.facePipee = facePipee;
	}

}
