package fr.rtgr.jeudedes.modele.utils;
import fr.rtgr.jeudedes.modele.De;

public class Gobelet {
	private De de1 = new De(); // attribut
	private De de2 = new De(); // attribut

	public int getValeurFace() { // m�thode
		return de1.getValeurFace() + de2.getValeurFace();
	}

	public void lancer() { // m�thode
		de1.lancer();
		de2.lancer();
	}
}
