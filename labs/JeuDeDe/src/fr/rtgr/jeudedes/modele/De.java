package fr.rtgr.jeudedes.modele;
public class De {
	/*
	 * Deux attributs pour la classe De 
	 * valeurFace -> la face du d� 
	 * MAX_LANCER -> constante pr�cisant le nombre de face maximum du d�
	 */
	private int valeurFace;
	private final int MAX_LANCER = 6;

	public De() {
		lancer();
	}

	/**
	 * @param valeur
	 *            -> face par d�faut du d�
	 */
	public De(int valeurFace) {
		this.valeurFace = valeurFace;
	}

	/**
	 * @author anne-lise.dubas
	 * @category comportement du lancer d'un d� � 6 faces
	 */
	public void lancer() {
		valeurFace = (int) (Math.random() * MAX_LANCER + 1);
	}

	// Accesseur de l'attribut valeurFace
	public int getValeurFace() {
		return valeurFace;
	}

	protected void setValeurFace(int valeurFace) {
		this.valeurFace = valeurFace;
	}

}
