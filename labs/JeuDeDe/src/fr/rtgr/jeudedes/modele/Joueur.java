package fr.rtgr.jeudedes.modele;
public final class Joueur {
	private final String nom;
	private int score;
	private final int NB_REGLE_GAGNANT = 7;

	public Joueur(final String nom) {
		this.nom = nom;
		score = 0;
	}

	public void aTonTour(De de1, De de2) {
		de1.lancer();
		de2.lancer();
		if (de1.getValeurFace() + de2.getValeurFace() >= NB_REGLE_GAGNANT) {
			incrementerScore();
		}
	}

	private void incrementerScore() {
		score++;
	}

	public String getNom() {
		return nom;
	}

	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}

}
