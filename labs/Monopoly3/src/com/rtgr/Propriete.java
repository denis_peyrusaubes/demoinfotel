package com.rtgr;

public abstract class Propriete extends Case{
	private int prixAchat;
	private Joueur proprietaire;

	public Propriete(String nom,int prixAchat){
		super(nom);
		this.prixAchat = prixAchat;
	}
	
	private boolean isOwned(){
		return proprietaire!=null;
	}

	public void sArreter(Joueur j) {
		
		if ( isOwned() ) {
			int loyer = calculerLoyer();
			j.debit(loyer);
			proprietaire.credit(loyer);
		} else {
			if ( j.getSolde() > prixAchat) {
				proprietaire = j;
				j.addPropriete(this);
			} else {
				System.out.println("Pas assez d'argent pour acheter");
			}
		}
	}

	public abstract int calculerLoyer();
	
	public boolean isGare() {
		return false;
	}

	public Joueur getProprietaire() {
		return proprietaire;
	}
	
	
}
