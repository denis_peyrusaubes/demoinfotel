package com.rtgr;

public class Plateau {
	private Case[] cases  = new Case[40];

	public Plateau() {
		
		
		for (int i = 0; i < 40; i++) {
			switch (i) {
			case 0:
				cases[i] = new Depart();
				break;
			case 4:
				cases[i] = new Impot();
				break;
			case 38:
				cases[i] = new Luxe();
				break;
			case 5:
			case 15:
			case 25:
			case 35:
				cases[i] = new Gare("Gare" + i);
				break;
			case 12:
			case 28:
				cases[i] = new Compagnie("Compagnie" + i,
						150);
				break;
			case 1:
			case 3:
			case 6:
			case 8:
			case 9:
			case 11:
			case 13:
			case 14:
			case 16:
			case 18:
			case 19:
			case 21:
			case 23:
			case 24:
			case 26:
			case 27:
			case 29:
			case 31:
			case 32:
			case 34:
			case 37:
			case 39:
				cases[i] = new Lot("Lot" + i, 40*i+10);
				break;
			default:
				cases[i] = new Case("Case" + i);
				break;
			}
		}
			
		
		for (int i=0;i<cases.length-1;i++) {
			cases[i].setCaseSuivante(cases[i+1]);

		}
		
		cases[cases.length-1].setCaseSuivante(cases[0]);
	}
	
	
	
	public Case getCaseDepart() {
		return cases[0];
	}

}
