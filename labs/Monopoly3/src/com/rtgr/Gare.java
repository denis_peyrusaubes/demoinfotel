package com.rtgr;

public class Gare extends Propriete {

	public Gare(String nom) {
		super(nom,200);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int calculerLoyer() {
		Joueur proprio = getProprietaire();
		
		return 25* proprio.getNbGare();
	}

	@Override
	public boolean isGare() {
		return true;
	}




}
