package com.rtgr;

import java.util.ArrayList;
import java.util.List;

public class Joueur {
	private java.lang.String nom;
	private Case position;
	private double solde;
	private List<Propriete> proprietes;


	public Joueur(String nom, Case position) {
		this.nom = nom;
		this.position = position;
		this.solde = 1500;
		proprietes = new ArrayList<Propriete>();
	}

	public void addPropriete(Propriete p) {
		proprietes.add(p);
	}
	
	public String getNom() {
		return nom;
	}


	public void jouer() {
		
		Gobelet gob = Gobelet.getInstance();
		gob.lancer();
		avancer(gob.getResultat());
		System.out.println("Le joueur "+nom+ " a tiré "+gob.getResultat()+". Il est sur la case "+position.getNom()+". \nIl possède "+getSolde()+"€ et les terrains suivants:");
		for (Propriete p:proprietes) {
			System.out.println("\t"+p.getNom());
		}
	}


	private void avancer(int resultat) {
		for (int i=0;i<resultat;i++) {
			step();
		}
	}


	private void step() {
		position = position.getCaseSuivante();

	}


	public double getSolde() {
		return solde;
	}

	public void credit(double m) {
		solde = solde+m;
	}
	public void debit(double m) {
		solde = solde-m;

	}

	@Override
	public String toString() {
		return "Nom:"+nom+"\nsolde:"+solde;
	}


	public int getNbGare(){
		int result = 0;
		for (Propriete p: proprietes) {
			if (p.isGare()) result++;
		}
		return result;
	}
}
