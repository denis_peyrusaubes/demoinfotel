package com.rtgr;

public class Impot extends Case {
	public Impot(){
		super("Impot");
	}
	
	public void sArreter(Joueur j) {
		super.sArreter(j);
		double montant = 200;
		
		if (j.getSolde()> 2000) {
			montant = 0.1*j.getSolde();
		} 
		
		j.debit(montant);
	}
}
