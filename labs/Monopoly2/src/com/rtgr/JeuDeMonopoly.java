package com.rtgr;

import java.util.ArrayList;
import java.util.List;

public class JeuDeMonopoly {
	
	private List<Joueur> joueurs ;
	private Plateau plateau;
	private int nbTour;
	
	public JeuDeMonopoly(int nbJoueurs, int nbTour) {
		
		
		plateau = new Plateau();
		Case caseDepart = plateau.getCaseDepart();
		
		
		joueurs = new ArrayList<Joueur>();
		for (int i = 0;i < nbJoueurs ; i++) {
			Joueur j = new Joueur("joueur" + i, caseDepart);
			joueurs.add(j);
		}
		
		this.nbTour = nbTour;
	}
	
	
	
	public void  jouer(){
		for (int i=0;i<nbTour;i++) {
			for (Joueur j:joueurs) {
				j.jouer();

			}
		}
		
			
	}



}
