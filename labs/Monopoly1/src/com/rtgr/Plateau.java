package com.rtgr;

public class Plateau {
	private Case[] cases  = new Case[40];

	public Plateau() {
		for (int i=0;i<40;i++) {
			cases[i] = new Case("case"+i);
		}
			
		
		for (int i=0;i<cases.length-1;i++) {
			cases[i].setCaseSuivante(cases[i+1]);

		}
		
		cases[cases.length-1].setCaseSuivante(cases[0]);
	}
	
	
	
	public Case getCaseDepart() {
		return cases[0];
	}

}
