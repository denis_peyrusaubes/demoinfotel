package com.rtgr;

import java.util.ArrayList;
import java.util.List;

public class Joueur {
	private String nom;
	private Case position;


	public Joueur(String nom, Case position) {
		this.nom = nom;
		this.position = position;
	}

	
	public String getNom() {
		return nom;
	}


	public void jouer() {
		
		Gobelet gob = Gobelet.getInstance();
		gob.lancer();
		avancer(gob.getResultat());
		System.out.println("Le joueur "+nom+ " a tiré "+gob.getResultat()+". Il est sur la case "+position.getNom());
	}


	private void avancer(int resultat) {
		for (int i=0;i<resultat;i++) {
			step();
		}
	}


	private void step() {
		position = position.getCaseSuivante();

	}



}
