package com.rtgr;


public class Gobelet {
	private De de1;
	private De de2;
	
	private static Gobelet theGobelet;

	private De[] des = {new De(),new De()};
	
	private Gobelet() {
		 de1 = new De();
		 de2 = new De();
	}
	
	
	public void lancer() {
		de1.lancer();
		de2.lancer();
	}

	public boolean isDouble() {
			return (de1.getValeurFace() == de2.getValeurFace());
	}
	
	
	public int getResultat() {
		return de1.getValeurFace() + de2.getValeurFace();
	}


	public static Gobelet getInstance() {
		if (theGobelet==null ) {
			theGobelet = new Gobelet();
		}
		return theGobelet;
	}
}
